﻿using InfoTrack.SearchEngine.Application.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using log4net.Config;
using InfoTrack.SearchEngine.Library.Models;
using InfoTrack.SearchEngine.Library;
using InfoTrack.SearchEngine.Application.Resources;
using InfoTrack.SearchEngine.Application.UI;
using System.Net;


namespace InfoTrack.SearchEngine.Application.Controllers
{
    public class SearchResultController : Controller
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(SearchResultController));

        private SearchResult _searchResult;
        //Using UNITY library (Initialised on start up) to enable dependency injection for the controllers
        //This is mainly used for unit testing the UI
        public SearchResultController(SearchResult searchResult)
        {
            this._searchResult = searchResult;
        }

        #region JASONCALLS
        [HttpPost]
        public JsonResult Search(string keyWord, string url, int limit)
        {
            try
            {
                //Calling the Search Engine Web Service
                log.Info(string.Format("start search for {0}/{1}", keyWord, url));
                IList<SearchResult> searchResult = SearchEngineResource.getSearchResultByQuery(keyWord, WebUtility.HtmlEncode(url), limit);
                log.Info(string.Format("finishe search for {0}/{1}", keyWord, url));

                //Using a decorator pattern to generate the content to be displayed
                SearchResultUi searchResultUi = new SearchResultUi();
                IUi searchResultMessage = new SearchResultMessageDecorator(searchResultUi, searchResult);
                IUi searchResultMessageGrid = new SearchResultGridDecorator(searchResultMessage, searchResult);
               
                return Json(searchResultMessageGrid.generateUi().ToString());
            }
            catch (Exception ex)
            {
                log.Warn(string.Format("Failed search {0}", ex.InnerException));
                throw ex;
            }
        }
        #endregion JASONCALLS
    }
}
