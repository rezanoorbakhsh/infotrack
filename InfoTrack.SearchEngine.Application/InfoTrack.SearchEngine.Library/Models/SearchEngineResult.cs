﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Library.Models
{
    public class SearchEngineResult
    {
        public int Id
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Link
        {
            get;
            set;
        }
    }
}
