﻿using InfoTrack.SearchEngine.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using InfoTrack.SearchEngine.Library.Models;
using InfoTrack.SearchEngine.Application.Models;

namespace InfoTrack.SearchEngine.Application.Resources
{
    public class SearchEngineResource
    {
        public static IList<SearchResult> getSearchResultByQuery(string keyWord, string url, int limit)
        {
            IList<SearchResult> searchResult = new List<SearchResult>();
           
           //Send the request
           using (HttpResponseMessage Res = WebApiClient.
                                                         getClient().
                                                         GetAsync(String.Format("api/SearchResult/{0}/{1}/{2}", keyWord, url, limit)).
                                                         Result
                 )
           {
               //Checking the response is successful or not which is sent using HttpClient  
               if (Res.IsSuccessStatusCode)
               {
                   //Storing the response details recieved from web api using NewTonsoft.Jason   
                   searchResult =  Res.Content.ReadAsAsync<IList<SearchResult>>().Result;
               }
           }
            return searchResult;
        }
    }
}