﻿function menuFix() {
    var sfEls = document.getElementById("TopMenu").getElementsByTagName("li");
    for (var i=0; i<sfEls.length; i++) {
        sfEls[i].onclick = function() {
            if(this.className.indexOf("sfhover") < 0)
            {
                //Expend this
                this.className+=(this.className.length>0? " ": "") + "sfhover";

                //Close Others
                var sfEls2 = document.getElementById("TopMenu").getElementsByTagName("li");
                for (var i=0; i<sfEls2.length; i++) {
                    if(sfEls2[i].className.indexOf("sfhover") >= 0 && sfEls2[i] != this)
                    {
                        sfEls2[i].className=sfEls2[i].className.replace(new RegExp("( ?|^)sfhover\\b"),"");
                    }
                }
            }
            else
            {
                this.className=this.className.replace(new RegExp("( ?|^)sfhover\\b"),"");
            }
        }
    }
}
window.onload=menuFix;