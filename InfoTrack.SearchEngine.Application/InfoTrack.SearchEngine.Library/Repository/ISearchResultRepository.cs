﻿using InfoTrack.SearchEngine.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoTrack.SearchEngine.Application.Models
{
    public interface ISearchResultRepository
    {
        IEnumerable<SearchEngineResult> GetAll();
        SearchEngineResult Add(SearchEngineResult item);
    }
}