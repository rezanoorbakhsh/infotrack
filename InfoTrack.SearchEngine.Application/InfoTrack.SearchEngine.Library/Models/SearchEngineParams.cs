﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Library.Models
{
    public class SearchEngineParams
    {
        public string apiKey
        {
            get;
            set;
        }

        public string searchEngineId
        {
            get;
            set;
        }

        public string query
        {
            get;
            set;
        }

        public int limit
        {
            get;
            set;
        }

        public bool validate()
        {
            if (apiKey == null || 
                searchEngineId == null || 
                query == null || 
                limit == 0)
            {
                return false;
            }
            return true;
        }
    }
}
