﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Library.Models
{
    public class SearchEngineResults
    {
        private IList<SearchEngineResult> _searchEngineResults = new List<SearchEngineResult>();

        public IList<SearchEngineResult> searchEngineResults
        {
            get { return _searchEngineResults; }
            set { _searchEngineResults = value; }
        }

        public void add(SearchEngineResult searchEngineResult) {
            _searchEngineResults.Add(searchEngineResult);
        }
    }
}
