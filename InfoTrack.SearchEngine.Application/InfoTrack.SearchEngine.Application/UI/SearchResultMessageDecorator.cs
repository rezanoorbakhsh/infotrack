﻿using InfoTrack.SearchEngine.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InfoTrack.SearchEngine.Application.UI
{
    class SearchResultMessageDecorator : UiDecorator
    {
        private IUi searchResultMessageDecorator;
        private IList<SearchResult> searchResult;

        public SearchResultMessageDecorator(IUi searchResultMessageDecorator, IList<SearchResult> searchResult)
            : base(searchResultMessageDecorator) 
        {
            this.searchResultMessageDecorator = searchResultMessageDecorator;
            this.searchResult = searchResult;
        }

        public override StringBuilder generateUi() 
        {
            return searchResultMessageDecorator.generateUi().Append(addMessage());
        }

        private StringBuilder addMessage()
        {
            StringBuilder resultHtml = new StringBuilder();
            resultHtml.Append("<table class=\"table table-striped\">");
            resultHtml.Append("<tr>");
            resultHtml.Append("<th>" + this.searchResult.Count + " records found matching with your key word! Here is the list</th>");
            resultHtml.Append("</tr>");
            resultHtml.Append("</table>");

            return resultHtml;
        }
    }
}