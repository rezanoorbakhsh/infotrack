﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InfoTrack.SearchEngine.Service;
using InfoTrack.SearchEngine.Service.Controllers;
using InfoTrack.SearchEngine.Service.Models;
using InfoTrack.SearchEngine.Service.Tests.Repository;

namespace InfoTrack.SearchEngine.Service.Tests.Controllers
{
    [TestClass]
    public class SearchResultControllerTest
    {
        [TestMethod]
        public void Get()
        {
            //Initiate
            SearchResultController controller = new SearchResultController();

            //Run unit test on SearchResult Controller with param repository
            SearchEngineParamRepository SeParamRep = new SearchEngineParamRepository();
            foreach (var v in SeParamRep.getParamsDictionary())
            {
                IList<SearchResult> result = controller.GetAllSearchResults(v.Value[0].ToString(),
                                                                            v.Value[1].ToString(),
                                                                            (int)v.Value[1]);

                // Assert
                //Here we should write the result into a file
                //To simplify for this case we just output it to our console
                foreach(SearchResult sr in result) {
                    Assert.IsNull(sr);
                    Console.WriteLine("Record {0} with link = {1} and title = {2} \n" , sr.Id, sr.Link, sr.Title);
                }
            }
            
        }
    }
}
