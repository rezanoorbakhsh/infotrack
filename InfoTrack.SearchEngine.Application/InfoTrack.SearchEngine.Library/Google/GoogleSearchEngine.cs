﻿using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;
using Google.Apis.Services;
using InfoTrack.SearchEngine.Library.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Library.Google
{
    /*
      A google search engine class utilizing google.api.customsearch.v1
     * REFERENCES: http://hintdesk.com/c-how-to-use-google-custom-search-api/
    */
    class GoogleSearchEngine : ISearchEngine
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(GoogleSearchEngine));
        public SearchEngineResults Execute(SearchEngineParams p)
        {
            var customSearchService = new CustomsearchService(new BaseClientService.Initializer { ApiKey = p.apiKey });
            var listRequest = customSearchService.Cse.List(p.query);
            listRequest.Cx = p.searchEngineId;

            SearchEngineResults searchResults = new SearchEngineResults();
            SearchEngineResult searchResult = null;

            try
            {
                IList<Result> paging = new List<Result>();
                var count = 0;
                int id = 0;
                while (paging != null)
                {
                    listRequest.Start = count * 10 + 1;
                    paging = listRequest.Execute().Items;
                    if (paging != null)
                    {
                        foreach (var item in paging)
                        {
                            id++;
                            searchResult = new SearchEngineResult();
                            searchResult.Id = id;
                            searchResult.Title = item.Title.Trim();
                            searchResult.Link = WebUtility.HtmlEncode(item.Link.Trim());

                            searchResults.add(searchResult);
                            if (id >= p.limit) return searchResults;
                        }
                        count++;
                        
                    }
         
                }
            }
            catch(Exception ex) {
                log.Info(string.Format("Exception occurred during calling google api {0}", ex.Message));
            }
            return searchResults;
            
        }
    }
}
