﻿using InfoTrack.SearchEngine.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InfoTrack.SearchEngine.Application.UI
{
    class SearchResultGridDecorator : UiDecorator
    {
        private IUi searchResultGridDecorator;
        private IList<SearchResult> searchResult;

        public SearchResultGridDecorator(IUi searchResultGridDecorator, IList<SearchResult> searchResult)
            : base(searchResultGridDecorator) 
        {
            this.searchResultGridDecorator = searchResultGridDecorator;
            this.searchResult = searchResult;
        }

        public override StringBuilder generateUi() 
        {
            return searchResultGridDecorator.generateUi().Append(addTable());
        }

        private StringBuilder addTable()
        {
            StringBuilder resultHtml = new StringBuilder();

            resultHtml.Append("<table class=\"table table-striped\">");
            resultHtml.Append("<tr>");
            resultHtml.Append("<th>Number</th>");
            resultHtml.Append("<th>Title</th>");
            resultHtml.Append("<th>Link</th>");
            resultHtml.Append("</tr>");

            string rowClor = "D9E6C4";
            int i = 0;
            foreach (var item in this.searchResult)
            {
                i++;
                if (rowClor == "#ffffff")
                {
                    rowClor = "#D9E6C4";
                }
                else
                {
                    rowClor = "#ffffff";
                }
                resultHtml.Append("<tr style=\"background-color:" + rowClor + ";\">");
                resultHtml.Append("<td>" + item.Id + "</td>");
                resultHtml.Append("<td>" + item.Title + "</td>");
                resultHtml.Append("<td>" + item.Link + "</td>");

                resultHtml.Append("</tr>");
            }
            resultHtml.Append("</table>");

            return resultHtml;
        }
    }
}