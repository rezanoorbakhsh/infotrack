﻿using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;
using Google.Apis.Services;
using InfoTrack.SearchEngine.Library;
using InfoTrack.SearchEngine.Library.Models;
using InfoTrack.SearchEngine.Service.Cashe;
using InfoTrack.SearchEngine.Service.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;



namespace InfoTrack.SearchEngine.Service.Controllers
{
    public class SearchResultController : ApiController
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(SearchResultController));

        #region SERVICES
        public IList<SearchResult> GetAllSearchResults(string keyWord, string url, int limit)
        {
            SearchEngineParams parameters = new GoogleSearchEngineParams();
            List<SearchEngineResult> filteredSearchEngineResultByUrl = null;
            //read cashe for settings
            //Populate the parameters
            parameters.apiKey = CasheSetting<string, string>.Get()[Constant.Constant.apiKey].ToString();
            parameters.searchEngineId = CasheSetting<string, string>.Get()[Constant.Constant.searchEngineId].ToString();
            parameters.query = keyWord;
            parameters.limit = limit;

            try
            {   //Validate the parameters before doing anything. 
                //A simple validation making sure we have all required parameters
                if (parameters.validate())
                {
                    SearchEngineFactory searchFactory = new SearchEngineFactory(SearchEngineProvider.Code.GOOGLE);
                    //Do the seach by calling the search engine executer
                    SearchEngineResults SearchEngineResult = searchFactory.Execute(parameters);
                    if (SearchEngineResult != null)
                    {
                        //Filter the results by url
                        //We are only interested in the links contain the passed URL
                        filteredSearchEngineResultByUrl = SearchEngineResult.searchEngineResults.Where(item =>
                                                                                                       item.Link.ToLower().
                                                                                                       Contains(url.
                                                                                                       ToLower())).ToList();
                    }
                    else
                    {
                        throw new Exception("No result returned from the search engine");
                    }
                }
                else
                {
                    throw new ArgumentException("One of the parameters for the search engine is null.");
                }
            }
            catch(ArgumentNullException argEx) {
                log.Warn(string.Format(argEx.Message));
            }
            catch(Exception genEx) {
                log.Warn(string.Format(genEx.Message));
            }
             return Utils.Utils.ToSearchResult(filteredSearchEngineResultByUrl);
        }

        #endregion SERVICES
    }
}
     
