﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoTrack.SearchEngine.Application.Models
{
    public interface ISearchResultRepository
    {
        IEnumerable<SearchResult> GetAll();
        SearchResult Add(SearchResult item);
    }
}