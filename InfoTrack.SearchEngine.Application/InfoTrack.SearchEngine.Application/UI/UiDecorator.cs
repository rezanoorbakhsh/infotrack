﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InfoTrack.SearchEngine.Application.UI
{
    /*Ui decorator implements Ui interfaace and look after 
    building the ui and decorate it upon client request. 
     It utilises the decorator design pattern*/
    class UiDecorator : IUi
    {
        private IUi ui;

        public UiDecorator(IUi ui)
        {
            this.ui = ui;
        }

        public virtual StringBuilder generateUi()
        {
            return ui.generateUi();
        }
    }
}