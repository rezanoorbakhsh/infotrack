﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoTrack.SearchEngine.Service.Cashe
{
    /*A simple class used for cashing the settings
    Ideally we need separate classes for each cashe context (settings, objects...)
    However, we can use one single class for all context using a key Id.
    Here, we go for the first option, and craete separate cashe for each context.
    */
    class CasheSetting<k, v> 
    {
        private static Dictionary<k, v> cashePool;
        public static void Put(k key, v value)
        {
            if (cashePool == null)
            {
                cashePool = new Dictionary<k, v>();
                cashePool.Add(key, value);
            }
            else
            {
                cashePool.Add(key, value);
            }
        }

        public static Dictionary<k, v> Get()
        {
            return cashePool;
        }
    }
}