﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InfoTrack.SearchEngine.Application.UI
{
    interface IUi
    {
        StringBuilder generateUi();
    }
}