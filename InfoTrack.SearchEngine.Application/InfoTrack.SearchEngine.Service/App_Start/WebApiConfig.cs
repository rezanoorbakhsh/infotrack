﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using InfoTrack.SearchEngine.Service.Cashe;
using System.Configuration;

namespace InfoTrack.SearchEngine.Service
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Book the search engine service
            config.Routes.MapHttpRoute(
                name: "SearchResult",
                routeTemplate: "api/{controller}/{keyWord}/{url}/{limit}"
             
            );
            config.EnableSystemDiagnosticsTracing();
            //Cache the setting at the start up
            intitSetting();
        }

        private static void intitSetting() {
            CasheSetting<string, string>.Put(Constant.Constant.apiKey, ConfigurationManager.AppSettings[Constant.Constant.apiKey]);
            CasheSetting<string, string>.Put(Constant.Constant.searchEngineId, ConfigurationManager.AppSettings[Constant.Constant.searchEngineId]);
        }
    }
}
