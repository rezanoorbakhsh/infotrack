﻿using InfoTrack.SearchEngine.Library.Google;
using InfoTrack.SearchEngine.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Library
{
    /*This is mostly a wrapper class for the search engine
     than a factory class*/
    public class SearchEngineFactory
    {
        ISearchEngine _searchEngine;
        public SearchEngineFactory(SearchEngineProvider.Code searchEngineProvider)
        {
            switch (searchEngineProvider) {
                case SearchEngineProvider.Code.GOOGLE:
                    this._searchEngine = new GoogleSearchEngine();
                    break;
                default:
                    throw new NotImplementedException();
            }
            
        }

        public SearchEngineResults Execute(SearchEngineParams p)
        {
            return this._searchEngine.Execute(p);
        }
    }
}
