﻿using InfoTrack.SearchEngine.Library.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoTrack.SearchEngine.Application.Models
{
    //A simple repository class for unit testing
    public class SearchResultRepository : ISearchResultRepository
    {
        private List<SearchEngineResult> searchResults = new List<SearchEngineResult>();
        private int _nextId = 1;

        public SearchResultRepository()
        {
            // Add SearchResult for the Demonstration
            Add(new SearchEngineResult { Id = 0, Link = "InfoTrack.com.au", Title = "Info Track" });
            Add(new SearchEngineResult { Id = 1, Link = "enspec.com.au", Title = "Enspec Engineering" });
            Add(new SearchEngineResult { Id = 2, Link = "OneVue.com.au", Title = "OneVue Financial" });
            Add(new SearchEngineResult { Id = 2, Link = "infotrack.com.au", Title = "InfoTrack contact us" });
        }

        public IEnumerable<SearchEngineResult> GetAll()
        {
            
            return searchResults;
        }

        public SearchEngineResult Add(SearchEngineResult item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            searchResults.Add(item);
            return item;
        }
       
    }
}