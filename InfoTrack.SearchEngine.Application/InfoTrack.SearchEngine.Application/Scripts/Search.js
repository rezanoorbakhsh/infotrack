﻿
$(document).ready(function () {

    $("#search").click(function () {
        var keyWord = $('#keyWord').val();
        var url = encodeURIComponent($("#url").val());
        var limit = $("#limit").val();
        
        $("#waite").show();

        if (keyWord.length == 0 || url.length == 0 || limit == 0) {
            alert("Please enter the values");
            return;
        }
        

        $.ajax({
            type: "POST",
            data: "keyWord=" + keyWord + "&url=" + url + "&limit=" + limit,
            url: "../SearchResult/Search/",
            dataType: "json",
            success: function (result) {
                $("#resultDiv").html(result);
                $("#wait").css("display", "none");
            }
        });
    });


    $(document).ajaxStart(function () {
        // show loader on start
        $("#wait").css("display", "block");
    }).ajaxSuccess(function () {
        // hide loader on success
        $("#wait").css("display", "none");
    });
   
});
