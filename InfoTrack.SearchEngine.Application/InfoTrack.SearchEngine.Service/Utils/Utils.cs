﻿using InfoTrack.SearchEngine.Library.Models;
using InfoTrack.SearchEngine.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoTrack.SearchEngine.Service.Utils
{
    public static class Utils 
    {
        public static IList<SearchResult> ToSearchResult(IList<SearchEngineResult> searchEngineResult)
        {
            IList<SearchResult> filteredResults = new List<SearchResult>();
            SearchResult filteredResult = null;
            foreach (SearchEngineResult sr in searchEngineResult)
            {
                filteredResult = new SearchResult();
                filteredResult.Id = sr.Id;
                filteredResult.Link = sr.Link;
                filteredResult.Title = sr.Title;

                filteredResults.Add(filteredResult);
            }
            return filteredResults;
        }
    }
}