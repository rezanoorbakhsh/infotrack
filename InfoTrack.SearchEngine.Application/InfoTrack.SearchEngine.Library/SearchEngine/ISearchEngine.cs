﻿using InfoTrack.SearchEngine.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Library
{
    /*It is important that we wrap all the search engines
    It may not be really necessary now as we have one search engine,
    what if we add a new search engine in the future
     */
    interface ISearchEngine
    {
         SearchEngineResults Execute(SearchEngineParams searchEngineParams);
    }
}
