﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Configuration;
using System.Net.Http.Headers;


namespace InfoTrack.SearchEngine.Application.Resources
{
    /*A Singleton class looking 
     * after providing the Web Api 
     * HttpClient */
    public class WebApiClient
    {
        private static string baseUrl = ConfigurationManager.AppSettings["InfoTrackWebApiUri"];
        private static volatile HttpClient client = null;
        private static object syncLock = new object();

        public static HttpClient getClient()
        {
            //Make sure of thread safe
            lock (syncLock) 
            //Simple lazy initialization
            if (client == null)
            {
                client = new HttpClient();
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                return client;
            }
            else
            {
                return client;
            }

        }

        private WebApiClient()
        {

        }
    }
}