﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoTrack.SearchEngine.Service.Tests.Repository
{
    class SearchEngineParamRepository
    {
        private Dictionary<int, List<Object>> paramsDictionary = new Dictionary<int,List<object>>();

        public SearchEngineParamRepository()
        {
            // Add test cases
            Add(0 , new List<object> { "InfoTrack", "InforTrack", 10});
            Add(1, new List<object> {"InfoTrack2", "InforTrack2", 10});
            Add(2, new List<object> {"InfoTrack4", "InforTrack3", 10});
         
        }

        public Dictionary<int, List<Object>> Add(int id, List<object> paramList)
        {
            paramsDictionary.Add(id, paramList);
            return paramsDictionary;   
        }

        public Dictionary<int, List<Object>> getParamsDictionary()
        {
            return paramsDictionary;
        }

    }
}
